package me.foodcharity_dbproject;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextClock;
import android.widget.TextView;

import java.util.ArrayList;

public class DataAdapter extends BaseAdapter {

    ArrayList<String> data = new ArrayList<>();

    public DataAdapter(int mode) {
        switch (mode){
            //todo for each case send query and just fill the arrList
            case 0:

                break;
            case 1:

                break;

            case 2:

                break;

            case 3:

                break;

            case 4:

                break;
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView tv;
        if(parent!=null) {
             tv = new TextView(parent.getContext());
        }
        else tv = new TextView(null);
        tv.setText(data.get(position));
        return tv;
    }
}
