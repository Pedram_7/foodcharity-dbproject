package me.foodcharity_dbproject.Database;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

import me.foodcharity_dbproject.Entities.Address;
import me.foodcharity_dbproject.Entities.Charity;
import me.foodcharity_dbproject.Entities.Contract;
import me.foodcharity_dbproject.Entities.Delivery;
import me.foodcharity_dbproject.Entities.Donation;
import me.foodcharity_dbproject.Entities.Driver;
import me.foodcharity_dbproject.Entities.ExtraFood;
import me.foodcharity_dbproject.Entities.Guest;
import me.foodcharity_dbproject.Entities.Request;
import me.foodcharity_dbproject.Entities.Restaurant;

public class QueryMethods {
    static SQLiteDatabase db;

    public QueryMethods() {
        db = CharityDataBase.getDb();
    }

    public static ArrayList showCharityListQuery() {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.showCharityList(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("charity.name")));
                }
            }
        }
        c.close();
        return results;
    }

    public static ArrayList showFoodListQuery() {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.showFoodList(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("foodtype.name")));
                }
            }
        }
        c.close();
        return results;
    }



    public static ArrayList query1() {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.q1(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("avg(charity.coverage)")));
                }
            }
        }
        c.close();
        return results;
    }

    public static ArrayList query2() {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.q2(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("driver.username")));
                }
            }
        }
        c.close();
        return results;
    }


    public static void TriggerFood(){

    }

    public static String query3(String username) {
        String max = null, min = null;
        Cursor c = db.rawQuery(CharityDbHelper.q3(username), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    max = c.getString(c.getColumnIndex("max(delivery.rate)"));
                    min = c.getString(c.getColumnIndex("min(delivery.rate)"));
                }
            }
        }
        c.close();
        return username + " , " + max + " , " + min;
    }

    public static ArrayList query4() {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.q4(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("extrafood.resname")));
                }
            }
        }
        c.close();
        return results;
    }

    //TODO public static String query5(){}

    public static String query6() {
        String name = null;
        Cursor c = db.rawQuery(CharityDbHelper.q6(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    name = c.getString(c.getColumnIndex("charity.name"));
                }
            }
        }
        c.close();
        return name;
    }

    public static String query7() {
        String du = null, cn = null , rn = null;
        Cursor c = db.rawQuery(CharityDbHelper.q7(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    du = c.getString(c.getColumnIndex("delivery.driverUsrname"));
                    cn = c.getString(c.getColumnIndex("delivery.charName"));
                    rn = c.getString(c.getColumnIndex("extrafood.resName"));
                }
            }
        }
        c.close();
        return du + " , " + cn + " , " + rn;
    }

    public static ArrayList query8(String resName) {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.q8(resName), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("contract.resname")));
                }
            }
        }
        c.close();
        return results;
    }

    public static String query9(String username) {
        String name= null;
        Cursor c = db.rawQuery(CharityDbHelper.q9(username), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    name = c.getString(c.getColumnIndex("delivery.charname"));
                }
            }
        }
        c.close();
        return name;
    }

    //TODO public static String query10(String username){}

    public static String query11(String username) {
        int count= 0;
        Cursor c = db.rawQuery(CharityDbHelper.q11(), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    count = c.getInt(c.getColumnIndex("count(driver.username)"));
                }
            }
        }
        c.close();
        return String.valueOf(count);
    }

    public static ArrayList query12(String driverusername) {
        ArrayList<String> results = new ArrayList();
        Cursor c = db.rawQuery(CharityDbHelper.q12(driverusername), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    results.add(c.getString(c.getColumnIndex("extrafood.resname")));
                }
            }
        }
        c.close();
        return results;
    }

    public static void editProfileQuery(String user , String username , Object o){
        Cursor c = db.rawQuery(CharityDbHelper.editProfile(user , username , o), null);
        c.close();
    }

    public static int findUserForEditPasswordQuery ( String user , String username , String answer){
        int result = 0;
        Cursor c = db.rawQuery(CharityDbHelper.checkQuestionAnswer(user , username , answer), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    result = 1;
                }
            }
        }
        c.close();
        return result;
    }

    public static void editPasswordQuery(String user  , String username, String newP ){
        Cursor c = db.rawQuery(CharityDbHelper.editPassword(user , username , newP), null);
        c.close();
    }

    public static String signInQuery(String user , String username , String pass ) {
        int count= 0;
        Cursor c = db.rawQuery(CharityDbHelper.signIn(user , username , pass), null);
        if (c != null) {
            if (c.moveToFirst()) {
                while (c.moveToNext()) {
                    count = c.getInt(c.getColumnIndex("count("+user+".username)"));
                }
            }
        }
        c.close();
        return String.valueOf(count);
    }

    public static void deleteUserQuery(String user , String username){
        Cursor c = db.rawQuery(CharityDbHelper.deleteUser(user , username), null);
        c.close();
    }

    public static void deleteCRQuery(String user , String username){
        Cursor c = db.rawQuery(CharityDbHelper.deleteCR(user , username), null);
        c.close();
    }

    public static void deleteContractQuery(String charname , String resname){
        Cursor c = db.rawQuery(CharityDbHelper.deleteContract(charname , resname), null);
        c.close();
    }

    public static void deleteAddressQuery(String Lat , String Lang){
        Cursor c = db.rawQuery(CharityDbHelper.deleteAddress(Lat , Lang), null);
        c.close();
    }

    public static void createFoodTypeQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createFoodType(), null);
        c.close();
    }

    public static void createGuestQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createGuest(), null);
        c.close();
    }

    public static void createQuestionQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createQuestion(), null);
        c.close();
    }

    public static void createAdminQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createAdmin(), null);
        c.close();
    }

    public static void createcAdminQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createcAdmin(), null);
        c.close();
    }

    public static void createrAdminQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createrAdmin(), null);
        c.close();
    }

    public static void createِDriverQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createِDriver(), null);
        c.close();
    }

    public static void createAddressQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createAddress(), null);
        c.close();
    }

    public static void createRestaurantQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createRestaurant(), null);
        c.close();
    }

    public static void createCharityQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createCharity(), null);
        c.close();
    }

    public static void createContractQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createContract(), null);
        c.close();
    }

    public static void createRequesttQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createRequest(), null);
        c.close();
    }

    public static void createExtraFoodQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createExtraFood(), null);
        c.close();
    }

    public static void createDeliveryQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createDelivery(), null);
        c.close();
    }

    public static void createDonationQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.createDonation(), null);
        c.close();
    }

    public static void statusChangeCountQuery(String username){
        Cursor c = db.rawQuery(CharityDbHelper.statusChangeCount(username), null);
        c.close();
    }

    public static void resDeliverFoodQuery(String count , String username){
        Cursor c = db.rawQuery(CharityDbHelper.resDeliverFood(count , username), null);
        c.close();
    }

    public static void insertCharityQuery(Charity ch){
        Cursor c = db.rawQuery(CharityDbHelper.insertCharity(ch), null);
        c.close();
    }

    public static void insertRestaurantQuery(Restaurant r){
        Cursor c = db.rawQuery(CharityDbHelper.insertRestaurant(r), null);
        c.close();
    }

    public static void insertGuestQuery(Guest g){
        Cursor c = db.rawQuery(CharityDbHelper.insertGuest(g), null);
        c.close();
    }

    public static void insertContractQuery(Contract co){
        Cursor c = db.rawQuery(CharityDbHelper.insertcontract(co), null);
        c.close();
    }

    public static void insertRequestQuery(Request r){
        Cursor c = db.rawQuery(CharityDbHelper.inserrequest(r), null);
        c.close();
    }

    public static void insertExtraFoodQuery(ExtraFood e){
        Cursor c = db.rawQuery(CharityDbHelper.insertextrafood(e), null);
        c.close();
    }

    public static void insertDeliveryQuery(Delivery d){
        Cursor c = db.rawQuery(CharityDbHelper.insertdelivery(d), null);
        c.close();
    }

    public static void insertAddressQuery(Address a ){
        Cursor c = db.rawQuery(CharityDbHelper.insertaddress(a), null);
        c.close();
    }

    public static void insertDonationQuery(Donation d){
        Cursor c = db.rawQuery(CharityDbHelper.insertdonation(d), null);
        c.close();
    }
    public static void insertFoodtypeQuery(){
        Cursor c = db.rawQuery(CharityDbHelper.insertFoodType("PK"), null);
         c = db.rawQuery(CharityDbHelper.insertFoodType("PJ"), null);
         c = db.rawQuery(CharityDbHelper.insertFoodType("ES"), null);

        c.close();
    }

    public static void insertDriverQuery(Driver d){
        Cursor c = db.rawQuery(CharityDbHelper.insertDriver(d), null);
        c.close();
    }

    public static void insertrcAdminQuery(String user , String qnumber , String answer , String username , String password , String name){
        Cursor c = db.rawQuery(CharityDbHelper.insertrcAdmin(user , qnumber , answer , username , password , name), null);
        c.close();
    }







}
