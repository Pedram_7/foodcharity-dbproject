package me.foodcharity_dbproject.Database;


public class Constants {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "charity.db";
    public static final String CREATE = "charity.db";
    public static final String SP_NAME = "charity.db";

    public static String CT = "CREATE TABLE ";
    public static String NN = "NOT NULL ";
    public static String PK = "PRIMARY KEY ";
    public static String INT = "INTEGER";
    public static String FL = "FLOAT";
    public static String CHAR = "CHARACTER";
    public static String CONS = "CONSTRAINT ";


    private static final Constants ourInstance = new Constants();

    static Constants getInstance() {
        return ourInstance;
    }

    private Constants() {
    }
}