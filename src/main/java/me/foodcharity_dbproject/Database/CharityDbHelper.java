package me.foodcharity_dbproject.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;
import me.foodcharity_dbproject.Entities.Address;
import me.foodcharity_dbproject.Entities.Charity;
import me.foodcharity_dbproject.Entities.Contract;
import me.foodcharity_dbproject.Entities.Delivery;
import me.foodcharity_dbproject.Entities.Donation;
import me.foodcharity_dbproject.Entities.Driver;
import me.foodcharity_dbproject.Entities.ExtraFood;
import me.foodcharity_dbproject.Entities.Guest;
import me.foodcharity_dbproject.Entities.Request;
import me.foodcharity_dbproject.Entities.Restaurant;

public class CharityDbHelper extends SQLiteOpenHelper {


    CharityDbHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }



    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        DataBaseInit.createTables(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public static String checkQuestionAnswer (String user , String username ,String answer){
        String S = "SELECT count( + " + user + ".answer) FROM " + user + " WHERE " + user + ".answer = " + answer + " and " +
                user + ".username = " + username;
        return S;
    }

    public static String editProfile (String user , String username , Object o) {
        if(user == "rAdmin") {
            Restaurant r = (Restaurant) o;
            String s = "UPDATE restaurant SET area = " + r.getArea() + " , address = "
                    + r.getAddress() + "WHERE restaurant.name = " + username;
            return s;
        }

        else if(user == "cAdmin") {
            Charity c = (Charity) o;
            String s = "UPDATE charity SET area = " + c.getArea() + " , address = "
                    + c.getAddress() + "WHERE charity.name = " + username;
            return s;
        }

        else if(user == "driver") {
            Driver d = (Driver) o;
            String s = "UPDATE driver SET nId = " + d.getnId() + " , CarPlaque = "
                    + d.getPlaque() + " , FName = " + d.getName() + ", LName = " + d.getLastName()
                     + " ,CarColor = " + d.getCarColor() + "WHERE driver.name = " + username;
            return s;
        }

        return null;
    }

    public static String editPassword (String user , String username , String newP) {
        String s = "UPDATE " + user + " SET password = " + newP + " WHERE " + user + ".username = " + username;
        return s;
    }

    public static String signIn (String user , String username , String pass ){
        String s = "SELECT distinct count(" + user + ".username FROM " + user + " WHERE " + user + ".username = " + username + "and " +
                user + ".pssword =" + pass;
        return s;
    }

    public static String deleteUser (String user , String username) {
        String s = "DELETE FROM " + user + "WHERE " + user + ".username = " + username;
        return s;
    }

    public static String deleteCR (String user , String name) {
        String s = "DELETE FROM " + user + "WHERE " + user + ".name = " + name;
        return s;
    }

    public static String deleteContract (String charName , String resName) {
        String s = "DELETE FROM contract WHERE contract.charname = " + charName + "and contract.resname = " + resName;
        return s;
    }

    public static String deleteAddress (String lat , String lang) {
        String s = "DELETE FROM address WHERE address.Lat = " + lat + " and address.lang = " + lang;
        return s;
    }

    public static String statusChangeCount (String username){
        String s = "UPDATE driver" + " SET changeStatusCount = changeStatusCount + 1 WHERE driver.username = " + username;
        return s;
    }

    // SERACHES

    public static String q1 (){
        String s = "SELECT avg(charity.Coverage),charity.area FROM charity GROUP BY charity.area";
        return s;
    }

    public static String q2 () {
        String s = "SELECT driver.username FROM driver , delivery WHERE driver.username = delivery.driverUsrname GROUP BY driver.username" +
                "HAVING  avg(deliver.rate) > 4";
        return s;
    }

    public static String q3 (String username) {
        String s = "SELECT min(delivery.rate) , max(delivery.rate) , delivery.driverUsrname" + "" +
                "FROM delivery WHERE delivery.driverUsrname = " + username+ " GROUP BY delivery.driverUsrname";
        return s;
    }

    public static String q4 () {
        String s = "SELECT extrafood.resname " +
                "FROM extrafood " +
                "GROUP BY extrafood.resname " +
                "ORDER BY (SELECT extrafood.count FROM extrafood)";
        return s;
    }

    // TODO public static String q5 () {}

    public static String q6 () {
        String s = "SELECT charity.name FROM charity , donation , contract" +
                "WHERE charity.name = donation.charname and charity.name = contract.charname" +
                "GROUP BY charity.name" +
                "HAVING max(charity.coverage) and min(donation.count) and count( contract.resname )"+
                "< all (SELECT count(contract.resname) FROM contract )";
        return s;
    }

    public static String q7 (){
        String s = "SELECT delivery.driverUsrname , delivery.charName, extrafood.resName" +
                "FROM delivery , extrafood WHERE delivery.foodtype = extrafood.type and extrafood.date > ( SELECT DATE ( ‘now’ , ‘-1 day’)) HAVING delivery.rate > avg(delivery.rate)";
        return s;
    }

    public static String q8 (String resname){
        String s = "SELECT contract.resname FROM contract " +
                "GROUP BY contract.resname HAVING contract.charname = ( SELECT contract.charname" +
                "FROM contract WHERE contract.resname = " + resname;
        return s;
    }

    public static String q9 (String resname) {
        String s = "SELECT delivery.charname FROM delivery , extrafood " +
                "WHERE delivery.foodtype = extrafood.type and extrafood.resname = " + resname +
                " GROUP BY delivery.charname HAVING min(extrafood.count)";
        return s;
    }

    /* TODO public static String q10 (String resname){
        String s = "SELECT charity.name FROM charity , contract WHERE contract.resname = " + resname + "and charity.name NOT IN" +
                "(SELECT contract.charname FORM contract WHERE contract.resname = " + resname + ")";
        return s;
    }*/

    public static String q11 () {
        String s = "SELECT count(driver.username) " +
                "FROM driver " +
                "GROUP BY driver.area " +
                "Having driver.status = 1";
        return s;
    }

    public static String q12 (String driverusername){
        String s = "SELECT extrafood.resname " +
                "FROM delivery , extrafood " +
                "WHERE delivery.foodtype = extrafood.type and delivery.driverUsrname = " + driverusername+
                " ORDER BY ( SELECT count(delivery.count FROM delivery)";
        return s;
    }

    public static String showCharityList(){
        String s = "SELECT distinct charity.name" +
                "FROM charity";
        return s;
    }

    public static String showFoodList(){
        String s = "SELECT distinct foodtype.name" +
                "FROM foodtype";
        return s;
    }

    //Inserts
    public static String insertDriver (Driver d) {
        String s = "INSERT INTO driver " +
                "Values(" + d.getnId() + "," + d.getPlaque() + "," + d.getStatus() + ", " + d.getName() + "," +
        d.getLastName() + "," + d.getBd() + "," + d.getArea() + "," +d.getCarColor() + ", " +
        d.getLat() + "," + d.getLang() + ")";
        return s;
    }
    public static String insertrcAdmin (String user , String qnumber , String answer , String username , String password , String name) {

            String s = "INSERT INTO " + user +
                    "Values(" +  qnumber + "," + answer + "," + username + "," + password + "," + name + ")";
            return s;
    }

    public static String insertRestaurant  (Restaurant r) {
        String s = "INSERT INTO restaurant " +
                "Values( " + r.getName() + "," +r.getAddress() + "," +r.getArea()+")";
        return s;
    }
    public static String insertCharity (Charity c) {
        String s = "INSERT INTO charity " +
                "Values( " + c.getName() + "," +c.getAddress() + "," +c.getArea()+ "," +c.getCoverage() + "," +c.getFoundYear() +")";
        return s;
    }
    public static String insertGuest (Guest g) {
        String s = "INSERT INTO guest" +
                "Values(" +g.getfName()+"," + g.getlName()+")";
        return s;
    }
    public static String insertcontract  (Contract c) {
        String s = "INSERT INTO contract" +
                "Values(" +c.getResName()+" ," + c.getCharName()+")";
        return s;
    }
    public static String inserrequest  (Request r) {
        String s = "INSERT INTO request " +
                "Values(" + r.getDate()+" ,"+ r.getCount() + ", " + r.getCharName() + ")";
        return s;
    }
    public static String insertextrafood  (ExtraFood e) {
        String s = "INSERT INTO extrafood  " +
                "Values(" + e.getCount()+ "," + e.getResName() + "," +e.getType() + ")";
        return s;
    }
    public static String insertdelivery   (Delivery d) {
        String s = "INSERT INTO delivery   " +
                "Values(" + d.getCharName() + "," + d.getDriverUsrname() + "," + d.getFoodtype() +
        "," + d.getResname() + "," + d.getDate()+ "," + d.getRate() + ")";
        return s;
    }
    public static String insertaddress   (Address a) {
        String s = "INSERT INTO address   " +
                "Values( " + a.getCity() + "," + a.getStreet() + "," +a.getPlaque()+ "," + a.getLat() +
        "," + a.getLang() + ")";
        return s;
    }
    public static String insertdonation   (Donation d) {
        String s = "INSERT INTO donation   " +
                "Values( " + d.getCharname() + "," + "SELECT DATE ('now')" + "," +d.getFoodtype()+ "," + d.getGuestFname() +
                "," + d.getGuestLname() + ")";
        return s;
    }
    public static String insertFoodType   (String name) {
        String s = "INSERT INTO foodtype   " +
                "Values( " + name +")";
        return s;
    }






    //Create Tables

    public static String createFoodType () {
        String s = Constants.CT + "foodtype ( Name " + Constants.CHAR + "(20) " + Constants.NN + Constants.PK + ")";
        return s;
    }

    public static String createGuest () {
        String s = Constants.CT + "guest ( FName " + Constants.CHAR + "(20)"+ Constants.NN + " , LName" + Constants.CHAR + "(20)" + Constants.NN + " , " + Constants.CONS + "guest_pk " +
                Constants.PK + "(FName , LName))";
        return s;
    }

    public static String createQuestion () {
        String s = Constants.CT + "question ( QNumber " + Constants.INT + Constants.NN + Constants.PK + " , Text " + Constants.CHAR +"(50)" + Constants.NN +")";
        return s;
    }

    public static String createAdmin () {
        String s = Constants.CT + "admin ( UserName " + Constants.CHAR + "(20) " + Constants.NN + Constants.PK +
                " , Password " + Constants.CHAR + " (15) " + Constants.NN  + ", QNumber_FK " + Constants.INT + Constants.NN + " , QAnswer " + Constants.CHAR + "(20)" + Constants.NN +")";
        return s;
    }

    public static String createcAdmin () {
        String s = Constants.CT + "cAdmin ( UserName " + Constants.CHAR + "(20) " + Constants.NN + Constants.PK +
                " , Password " + Constants.CHAR + " (15) " + Constants.NN  + ", QNumber_FK " + Constants.INT + Constants.NN + " , QAnswer " + Constants.CHAR + "(20)" + Constants.NN
                + " , CharityName_FK "+ Constants.CHAR + " (20)" + Constants.NN +")";
        return s;
    }

    public static String createrAdmin () {
        String s = Constants.CT + "rAdmin ( UserName " + Constants.CHAR + "(20) " + Constants.NN + Constants.PK +
                " , Password " + Constants.CHAR + " (15) " + Constants.NN  + ", QNumber_FK " + Constants.INT + Constants.NN + " , QAnswer " + Constants.CHAR + "(20)" + Constants.NN
                + " , RestaurantName_FK "+ Constants.CHAR + " (20)" + Constants.NN +")";
        return s;
    }

    public static String createِDriver () {
        String s = Constants.CT + "driver ( UserName " + Constants.CHAR + "(20) " + Constants.NN + Constants.PK +
                " , Password " + Constants.CHAR + " (15) " + Constants.NN + " , nId " +Constants.INT + Constants.NN + ", QNumber_FK " + Constants.INT + Constants.NN + " , QAnswer " + Constants.CHAR + "(20)" + Constants.NN
                + " , FName "+ Constants.CHAR + " (15)" + Constants.NN + " , LName "+ Constants.CHAR + " (15)" + Constants.NN + " , Lat " + Constants.FL + Constants.NN +
                ", Lang" + Constants.FL + Constants.NN + " , Area" + Constants.CHAR + "(20) , CarColor " + Constants.CHAR + "(10) , CarPlaque " + Constants.INT +")";
        return s;
    }

    public static String createAddress () {
        String s = Constants.CT + "address (Lat " + Constants.FL + Constants.NN + ", Lang" + Constants.FL + Constants.NN + " , City" + Constants.CHAR + "(10)" + Constants.NN + " , Plaque" + Constants.INT + Constants.NN
                + " , Street" + Constants.CHAR + "(20)" + Constants.NN + " , " + Constants.CONS + "address_pk " + Constants.PK + "(Lat , Lang))";
        return s;
    }

    public static String createRestaurant () {
        String s = Constants.CT + "restaurant ( Name " + Constants.CHAR + " (20) " + Constants.NN + Constants.PK + " , Lat " + Constants.FL + Constants.NN + ", Lang" + Constants.FL + Constants.NN +
                " , Area " + Constants.CHAR + " (20))";
        return s;
    }

    public static String createCharity () {
        String s = Constants.CT + "charity ( Name " + Constants.CHAR + " (20) " + Constants.NN + Constants.PK + " , Lat " + Constants.FL + Constants.NN + ", Lang" + Constants.FL + Constants.NN +
                " , FoundationYear" + Constants.INT + Constants.NN + ", Coverage " + Constants.INT + Constants.NN + " , Area " + Constants.CHAR + " (20))";
        return s;
    }

    public static String createContract () {
        String s = Constants.CT + "contract ( RestaurantName  " + Constants.CHAR + "(20)"+ Constants.NN + " , CharityName " + Constants.CHAR + "(20)" + Constants.NN + " , " + Constants.CONS +
                "contract_pk " + Constants.PK + "(RestaurantName , CharityName ))";
        return s;
    }

    public static String createRequest () {
        String s = Constants.CT + "request ( CharityName_FK  " + Constants.CHAR + "(20) " + Constants.NN + " , Count " + Constants.INT + Constants.NN + " , Date TEXT "+ Constants.NN + Constants.PK + ")";
        return s;
    }

    public static String createExtraFood () {
        String s = Constants.CT + "extrafood ( RestaurantName_FK  " + Constants.CHAR + "(20)"+ Constants.NN + " , FoodType_FK" + Constants.CHAR + "(20)" + Constants.NN +
                " , Count " + Constants.INT + Constants.NN +" , Date TEXT " + Constants.NN + " , " + Constants.CONS + "extrafood_pk " + Constants.PK +
                "(RestaurantName_FK , FoodType , Date))";
        return s;
    }

    public static String createDelivery() {
        String s = Constants.CT + "delivery ( FoodType_FK   " + Constants.CHAR + "(20)"+ Constants.NN + " , CharityName _FK " + Constants.CHAR + "(20)" + Constants.NN +
                " , Rate " + Constants.INT + Constants.NN + " , DriverUserName_FK " + Constants.CHAR + "(20)" + Constants.NN +" , "+ Constants.CONS + "delivery_pk " + Constants.PK +
                "(FoodType_FK , CharityName_FK , DriverUserName_FK))";
        return s;
    }

    public static String createDonation() {
        String s = Constants.CT + "donation ( FoodType_FK   " + Constants.CHAR + "(20)"+ Constants.NN + " , CharityName _FK " + Constants.CHAR + "(20)" + Constants.NN +
                " , GuestFName_FK " + Constants.CHAR + "(20)" + Constants.NN + " , GuestLName_FK " + Constants.CHAR + "(20)" + Constants.NN +
                " , Date TEXT " + Constants.NN + " , Count " + Constants.INT + Constants.NN +" , "+ Constants.CONS + "delivery_pk " + Constants.PK +
                "(FoodType_FK , CharityName_FK , DriverUserName_FK))";
        return s;
    }

    public static String resDeliverFood(String count , String resname){
        String s = "Update extrafood" +
                "set  Count = Count - " + count + "WHERE extrafood.resname = " + resname;
        return s;
    }

    public static String charRecivesFood(String count , String resname){
        String s = "Update request" +
                "set Count = Count - " + count + "WHERE extrafood.resname = " + resname;
        return s;
    }

}


