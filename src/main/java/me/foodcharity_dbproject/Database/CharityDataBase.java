package me.foodcharity_dbproject.Database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import me.foodcharity_dbproject.Entities.Charity;

import static me.foodcharity_dbproject.Database.Constants.DATABASE_NAME;
import static me.foodcharity_dbproject.Database.Constants.DATABASE_VERSION;

public class CharityDataBase {
    private static CharityDataBase charityDataBase = new CharityDataBase();
    private static CharityDbHelper dbHelper;

    public static SQLiteDatabase getDb() {
        return db;
    }

    private static SQLiteDatabase db;

    public CharityDataBase getCharityDataBase(){
        return charityDataBase;
    }


    public static void initialize(Context context){

        dbHelper = new CharityDbHelper(context,DATABASE_NAME,null,DATABASE_VERSION); //db helper creates tables on its create

        db = dbHelper.getWritableDatabase();

        QueryMethods.createِDriverQuery();
        QueryMethods.createAddressQuery();
        QueryMethods.createAdminQuery();
        QueryMethods.createContractQuery();
        QueryMethods.createDeliveryQuery();
        QueryMethods.createDonationQuery();
        QueryMethods.createRestaurantQuery();
        QueryMethods.createCharityQuery();
        QueryMethods.createQuestionQuery();
        QueryMethods.createFoodTypeQuery();
        QueryMethods.createExtraFoodQuery();
        QueryMethods.createcAdminQuery();
        QueryMethods.createrAdminQuery();

        QueryMethods

    }
}




