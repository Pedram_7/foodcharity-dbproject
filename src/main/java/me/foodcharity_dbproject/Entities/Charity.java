package me.foodcharity_dbproject.Entities;

public class Charity {

    String name;
    Address address;
    String area;
    int coverage , foundYear;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Charity(String name , Address address, String area, int coverage, int foundYear) {
        this.address = address;
        this.area = area;
        this.coverage = coverage;
        this.foundYear = foundYear;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public int getCoverage() {
        return coverage;
    }

    public void setCoverage(int coverage) {
        this.coverage = coverage;
    }

    public int getFoundYear() {
        return foundYear;
    }

    public void setFoundYear(int foundYear) {
        this.foundYear = foundYear;
    }
}
