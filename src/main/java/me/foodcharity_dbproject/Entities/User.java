package me.foodcharity_dbproject.Entities;

public class User {

    int qNumber;
    String answer;
    String username;
    String password;
    UserType type;

    public User(int qNumber, String answer, String username, String password, UserType type) {
        this.qNumber = qNumber;
        this.answer = answer;
        this.username = username;
        this.password = password;
        this.type = type;
    }

    public int getqNumber() {
        return qNumber;
    }

    public void setqNumber(int qNumber) {
        this.qNumber = qNumber;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserType getType() {
        return type;
    }

    public void setType(UserType type) {
        this.type = type;
    }
}
