package me.foodcharity_dbproject.Entities;

public class Restaurant {

    String name;
    Address address;
    String area;

    public Restaurant(String name, Address address, String area) {
        this.name = name;
        this.address = address;
        this.area = area;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
