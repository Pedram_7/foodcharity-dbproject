package me.foodcharity_dbproject.Entities;

public class Donation {

    String foodtype,charname,date,guestFname, guestLname;
    int rate;

    public Donation(String foodtype, String charname, String guestFname, String guestLname, int rate) {
        this.foodtype = foodtype;
        this.charname = charname;
        this.guestFname = guestFname;
        this.guestLname = guestLname;
        this.rate = rate;
    }

    public String getFoodtype() {
        return foodtype;
    }

    public void setFoodtype(String foodtype) {
        this.foodtype = foodtype;
    }

    public String getCharname() {
        return charname;
    }

    public void setCharname(String charname) {
        this.charname = charname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getGuestFname() {
        return guestFname;
    }

    public void setGuestFname(String guestFname) {
        this.guestFname = guestFname;
    }

    public String getGuestLname() {
        return guestLname;
    }

    public void setGuestLname(String guestLname) {
        this.guestLname = guestLname;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
