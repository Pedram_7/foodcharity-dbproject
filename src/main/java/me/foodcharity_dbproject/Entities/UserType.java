package me.foodcharity_dbproject.Entities;

public enum UserType {
    rAdmin,
    cAdmin,
    driver,
    sysAdmin,
}
