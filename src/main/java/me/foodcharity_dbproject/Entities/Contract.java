package me.foodcharity_dbproject.Entities;

public class Contract {
    String resName;
    String charName;

    public Contract(String resName, String charName) {
        this.resName = resName;
        this.charName = charName;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }
}
