package me.foodcharity_dbproject.Entities;

public class ExtraFood {
    String resName;
    String type;
    String date;
    int count;

    public ExtraFood(String resName, String type, String date, int count) {
        this.resName = resName;
        this.type = type;
        this.date = date;
        this.count = count;
    }

    public String getResName() {
        return resName;
    }

    public void setResName(String resName) {
        this.resName = resName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
