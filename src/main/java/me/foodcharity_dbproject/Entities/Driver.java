package me.foodcharity_dbproject.Entities;

public class Driver extends User {

    int nId,plaque,status; //status : 0 1 2 : 1 for available

    String name, lastName,  bd , area ,carColor;

    int changeStatusCount = 0;

    float lat , lang;

    public Driver(int qNumber, String answer, String username, String password, UserType type, int nId, int plaque, int status, String name, String lastName, String bd, String area, String carColor, float lat, float lang) {
        super(qNumber, answer, username, password, type);
        this.nId = nId;
        this.plaque = plaque;
        this.status = status;
        this.name = name;
        this.lastName = lastName;
        this.bd = bd;
        this.area = area;
        this.carColor = carColor;
        this.lat = lat;
        this.lang = lang;
    }


    public Driver(int qNumber, String answer, String username, String password, UserType type, int nId, int plaque, String name, String lastName, String bd, String area, String carColor) {
        super(qNumber, answer, username, password, type);
        this.nId = nId;
        this.plaque = plaque;
        this.name = name;
        this.lastName = lastName;
        this.bd = bd;
        this.area = area;
        this.carColor = carColor;
    }

    public int getnId() {
        return nId;
    }

    public void setnId(int nId) {
        this.nId = nId;
    }

    public int getPlaque() {
        return plaque;
    }

    public void setPlaque(int plaque) {
        this.plaque = plaque;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getBd() {
        return bd;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLang() {
        return lang;
    }

    public void setLang(float lang) {
        this.lang = lang;
    }
}
