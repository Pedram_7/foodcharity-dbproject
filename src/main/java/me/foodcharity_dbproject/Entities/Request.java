package me.foodcharity_dbproject.Entities;

public class Request {
    String charName , date;
    int count;

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Request(String charName, String date, int count) {
        this.charName = charName;
        this.date = date;
        this.count = count;
    }
}
