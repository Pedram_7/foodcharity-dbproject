package me.foodcharity_dbproject.Entities;

import java.util.ArrayList;

public class Guest {
    private String fName,lName;
    public ArrayList<Donation> donations = new ArrayList<>();

    public Guest(String fName, String lName) {
        this.fName = fName;
        this.lName = lName;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public ArrayList<Donation> getDonations() {
        return donations;
    }

    public void setDonations(ArrayList<Donation> donations) {
        this.donations = donations;
    }
}
