package me.foodcharity_dbproject.Entities;

public class Delivery {

    String charName, driverUsrname,foodtype,resname,date;
    int rate;

    public String getCharName() {
        return charName;
    }

    public void setCharName(String charName) {
        this.charName = charName;
    }

    public String getDriverUsrname() {
        return driverUsrname;
    }

    public void setDriverUsrname(String driverUsrname) {
        this.driverUsrname = driverUsrname;
    }

    public String getFoodtype() {
        return foodtype;
    }

    public void setFoodtype(String foodtype) {
        this.foodtype = foodtype;
    }

    public String getResname() {
        return resname;
    }

    public void setResname(String resname) {
        this.resname = resname;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
