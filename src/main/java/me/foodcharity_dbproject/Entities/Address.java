package me.foodcharity_dbproject.Entities;

public class Address {
    String city;
    String street;
    int plaque;

    float lat,lang;

    public Address(String city, String street, int plaque, float lat, float lang) {
        this.city = city;
        this.street = street;
        this.plaque = plaque;
        this.lat = lat;
        this.lang = lang;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getPlaque() {
        return plaque;
    }

    public void setPlaque(int plaque) {
        this.plaque = plaque;
    }

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLang() {
        return lang;
    }

    public void setLang(float lang) {
        this.lang = lang;
    }
}
