package me.foodcharity_dbproject.Activities;

import androidx.appcompat.app.AppCompatActivity;
import me.foodcharity_dbproject.Database.QueryMethods;
import me.foodcharity_dbproject.R;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

public class ReadDataActivity extends AppCompatActivity {

    ListView dataListRV;
    Button q1Btn,q2Btn,q3Btn,q4Btn,q5Btn,q6Btn,q7Btn,q8Btn,q9Btn;
    TextView dataTV1,dataTV2, dataTV3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_data);

        dataListRV = findViewById(R.id.dataListLV);

        q1Btn = findViewById(R.id.queryBtn);
        q2Btn = findViewById(R.id.queryBtn2);
        q3Btn = findViewById(R.id.queryBtn3);
        q4Btn = findViewById(R.id.q6);
        q5Btn = findViewById(R.id.queryBtn5);
        q6Btn = findViewById(R.id.queryBtn6);
        q7Btn = findViewById(R.id.queryBtn7);
        q8Btn = findViewById(R.id.queryBtn8);
        q9Btn = findViewById(R.id.queryBtn9);

        dataTV1 = findViewById(R.id.readDataTV);
        dataTV2 = findViewById(R.id.readDataTV2);
        dataTV3 = findViewById(R.id.readDataTV3);


        q1Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryMethods.query1();
            }
        });

        q3Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryMethods.query3("ali");
            }
        });
        q6Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataTV1.setText(QueryMethods.query6());
            }
        });

        q9Btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QueryMethods.query9()
            }
        });


    }
}
