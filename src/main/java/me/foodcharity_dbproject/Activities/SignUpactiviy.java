package me.foodcharity_dbproject.Activities;

import androidx.appcompat.app.AppCompatActivity;
import me.foodcharity_dbproject.Database.QueryMethods;
import me.foodcharity_dbproject.Entities.Address;
import me.foodcharity_dbproject.Entities.Charity;
import me.foodcharity_dbproject.Entities.Driver;
import me.foodcharity_dbproject.Entities.Restaurant;
import me.foodcharity_dbproject.Entities.UserType;
import me.foodcharity_dbproject.R;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class SignUpactiviy extends AppCompatActivity {


    EditText signUpUsrnameET, signUpPasswordET;
    EditText dNameET, dColorET, dIDET, dAreaET, dBdET, dPlateET, dLastNameET, dLatLang;
    EditText rNameET, rPlaqueET, rAreaET, rLatLangET, rCityET, rStreetET;
    EditText cNameET, cPlaqueET, cAreaET, cLatLangET, cCityET, cStreetET, cFoundET, cCoverageET;

    EditText qNumET,ansET;
    TextView qtv1,qtv2,qtv3;

    Button dSignupBtn, cSignupBtn, rSignupBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signUpUsrnameET = findViewById(R.id.sUUsernameET);
        signUpPasswordET = findViewById(R.id.sUpwdET);

        dNameET = findViewById(R.id.dNameET);
        dColorET = findViewById(R.id.dColorET);
        dIDET = findViewById(R.id.dIDET);
        dAreaET = findViewById(R.id.dAreaET);
        dBdET = findViewById(R.id.dBdET);
        dPlateET = findViewById(R.id.dPlateET);
        dLastNameET = findViewById(R.id.dLastNameET);
        dLatLang = findViewById(R.id.dLatLangET);

        cAreaET = findViewById(R.id.cAreaET);
        cCityET = findViewById(R.id.cCityET);
        cNameET = findViewById(R.id.cNameET);
        cPlaqueET = findViewById(R.id.cPlaqueET);
        cLatLangET = findViewById(R.id.cLatLangET);
        cStreetET = findViewById(R.id.cStreetET);
        cFoundET = findViewById(R.id.cFoundET);
        cCoverageET = findViewById(R.id.cCoverageET);

        rNameET = findViewById(R.id.rNameET);
        rPlaqueET = findViewById(R.id.rPlaqueET);
        rAreaET = findViewById(R.id.rAreaET);
        rLatLangET = findViewById(R.id.rLatLang);
        rCityET = findViewById(R.id.rCityET);
        rStreetET = findViewById(R.id.rStreetET);

        dSignupBtn = findViewById(R.id.driverRegisterBtn);
        rSignupBtn = findViewById(R.id.resSignupBtn);
        cSignupBtn = findViewById(R.id.charitySignupBtn);

        qNumET = findViewById(R.id.qNumberET);
        ansET = findViewById(R.id.signupAnswerET);

        qtv1 = findViewById(R.id.q1TV);
        qtv2 = findViewById(R.id.q2TV);
        qtv3 = findViewById(R.id.q3TV);

        dSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Driver d = readDriverData();
                QueryMethods.insertDriverQuery(d);
            }
        });

        rSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Restaurant r = readResData();
                QueryMethods.insertRestaurantQuery(r);
                QueryMethods.insertrcAdminQuery("rAdmin" , qNumET.getText().toString() , ansET.getText().toString() , signUpUsrnameET.getText().toString() , signUpPasswordET.getText().toString() ,r.getName() );
            }
        });

        cSignupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Charity c = readCharityData();
                QueryMethods.insertCharityQuery(c);
                QueryMethods.insertrcAdminQuery("cAdmin" , qNumET.getText().toString() , ansET.getText().toString() , signUpUsrnameET.getText().toString() , signUpPasswordET.getText().toString() ,c.getName() );
            }
        });

    }

    private Driver readDriverData(){

        String str =  dLatLang.getText().toString();
        float lat = Float.parseFloat(str.substring(0,str.indexOf(',')));
        float lang = Float.parseFloat(str.substring(str.indexOf(',')+1));
        return new Driver(Integer.parseInt(qNumET.getText().toString()),ansET.getText().toString(),signUpUsrnameET.getText().toString(),signUpPasswordET.getText().toString(), UserType.driver,Integer.parseInt(dIDET.getText().toString()),Integer.parseInt(dPlateET.getText().toString()),0,dNameET.getText().toString(),dLastNameET.getText().toString(),dBdET.getText().toString(),dAreaET.getText().toString(),dColorET.getText().toString(),lat,lang);
    }
    private Restaurant readResData(){
        String str =  rLatLangET.getText().toString();
        float lat = Float.parseFloat(str.substring(0,str.indexOf(',')));
        float lang = Float.parseFloat(str.substring(str.indexOf(',')+1));
        return new Restaurant(rNameET.getText().toString(),new Address(rCityET.getText().toString(),rStreetET.getText().toString(),Integer.parseInt(rPlaqueET.getText().toString()),lat,lang),rAreaET.getText().toString());
    }
    private Charity readCharityData(){
        String str =  cLatLangET.getText().toString();
        float lat = Float.parseFloat(str.substring(0,str.indexOf(',')));
        float lang = Float.parseFloat(str.substring(str.indexOf(',')+1));
        return new Charity(cNameET.getText().toString(),new Address(cCityET.getText().toString(),cStreetET.getText().toString(),Integer.parseInt(cPlaqueET.getText().toString()),lat,lang),cAreaET.getText().toString(),Integer.parseInt(cCoverageET.getText().toString()),Integer.parseInt(cFoundET.getText().toString()));
    }
}
