package me.foodcharity_dbproject.Activities;

import androidx.appcompat.app.AppCompatActivity;
import me.foodcharity_dbproject.Database.QueryMethods;
import me.foodcharity_dbproject.Entities.User;
import me.foodcharity_dbproject.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import javax.xml.datatype.Duration;

import static me.foodcharity_dbproject.Database.Constants.SP_NAME;

public class SignInActivity extends AppCompatActivity {

    EditText usrnameET;
    EditText pwET;
    Button doLoginBtn , cAdminBtn,driverLoginBtn,rAdminBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        final SharedPreferences sharedPreferences = getSharedPreferences(SP_NAME,MODE_PRIVATE);
        usrnameET = findViewById(R.id.usrnameET);
        pwET = findViewById(R.id.sUpwdET);

        doLoginBtn = findViewById(R.id.loginBtn);
        rAdminBtn = findViewById(R.id.rAdminLoginBtn);
        cAdminBtn =findViewById(R.id.cAdminLoginBtn);
        driverLoginBtn = findViewById(R.id.driverLoginBtn);

        doLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Integer.parseInt(QueryMethods.signInQuery("admin",usrnameET.getText().toString(),pwET.getText().toString()) )== 1){
                    sharedPreferences.edit().putString("curUsername" , usrnameET.getText().toString()).commit();
                    sharedPreferences.edit().putString("userType" , "admin").commit();
                }
                else Toast.makeText(SignInActivity.this,"boro oskol", Toast.LENGTH_SHORT).show();
            }
        });

        rAdminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Integer.parseInt(QueryMethods.signInQuery("rAdmin",usrnameET.getText().toString(),pwET.getText().toString()) )== 1){
                    sharedPreferences.edit().putString("curUsername" , usrnameET.getText().toString()).commit();
                    sharedPreferences.edit().putString("userType" , "rAdmin").commit();
                }
                else Toast.makeText(SignInActivity.this,"boro oskol", Toast.LENGTH_SHORT).show();
            }
        });

        cAdminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Integer.parseInt(QueryMethods.signInQuery("cAdmin",usrnameET.getText().toString(),pwET.getText().toString()) )== 1){
                    sharedPreferences.edit().putString("curUsername" , usrnameET.getText().toString()).commit();
                    sharedPreferences.edit().putString("userType" , "cAdmin").commit();
                }
                else Toast.makeText(SignInActivity.this,"boro oskol", Toast.LENGTH_SHORT).show();
            }
        });

        driverLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Integer.parseInt(QueryMethods.signInQuery("driver",usrnameET.getText().toString(),pwET.getText().toString()) )== 1){
                    sharedPreferences.edit().putString("curUsername" , usrnameET.getText().toString()).commit();
                    sharedPreferences.edit().putString("userType" , "driver").commit();
                }
                else Toast.makeText(SignInActivity.this,"boro oskol", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
