package me.foodcharity_dbproject.Activities;

import androidx.appcompat.app.AppCompatActivity;
import me.foodcharity_dbproject.Database.QueryMethods;
import me.foodcharity_dbproject.Entities.Address;
import me.foodcharity_dbproject.Entities.Charity;
import me.foodcharity_dbproject.Entities.Driver;
import me.foodcharity_dbproject.Entities.Restaurant;
import me.foodcharity_dbproject.Entities.UserType;
import me.foodcharity_dbproject.R;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static me.foodcharity_dbproject.Database.Constants.SP_NAME;

public class EditActivity extends AppCompatActivity {

    EditText nameET,plaqueET,foundET,coverageET,areaET,latLangET,cityET,streetET,answerET,newPwET;
    TextView questionTV;
    Button charityEditBtn , changePwBtn,driverBtn;

    EditText dNameET,dLnameET,dBdET,dPlateET,dColorET,dAreaET,dNidET;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        areaET = findViewById(R.id.area3editET);
        cityET =findViewById(R.id.city3editET);
        nameET =findViewById(R.id.name3editET);
        foundET = findViewById(R.id.cFoundET);
        latLangET = findViewById(R.id.latLang3editET);
        streetET = findViewById(R.id.street3editET);
        answerET =findViewById(R.id.answerET);
        newPwET=findViewById(R.id.newPwET);
        plaqueET =findViewById(R.id.plaque3editET);
        coverageET = findViewById(R.id.cCoverageET);
        questionTV = findViewById(R.id.questionTV);


        dNameET = findViewById(R.id.city3editET2);
        dLnameET = findViewById(R.id.city3editET3);
        dBdET = findViewById(R.id.city3editET5);
        dPlateET = findViewById(R.id.city3editET6);
        dColorET = findViewById(R.id.city3editET7);
        dAreaET = findViewById(R.id.city3editET8);
        dNidET = findViewById(R.id.city3editET9);


        charityEditBtn = findViewById(R.id.charityEditBtn);
        changePwBtn = findViewById(R.id.changepwBtn);
        driverBtn = findViewById(R.id.driverEditBtn);

        final SharedPreferences sharedPreferences = getSharedPreferences(SP_NAME,MODE_PRIVATE);

        charityEditBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str = latLangET.getText().toString();
                float lat = sharedPreferences.getFloat("lat",Float.parseFloat(str.substring(0,str.indexOf(','))));
                float lang = sharedPreferences.getFloat("lang",Float.parseFloat(str.substring(str.indexOf(',')+1)));
                if(sharedPreferences.getString("userType"," ").equals("cAdmin")){
                    Charity charity = new Charity(nameET.getText().toString(),new Address(cityET.getText().toString(),streetET.getText().toString(),Integer.parseInt(plaqueET.getText().toString()),lat,lang),areaET.getText().toString(),Integer.parseInt(coverageET.getText().toString()),Integer.parseInt(foundET.getText().toString()));
                    QueryMethods.editProfileQuery("cAdmin",sharedPreferences.getString("curUsername",""),charity);
                }
                else if(sharedPreferences.getString("userType"," ").equals("rAdmin")){
                    Restaurant restaurant = new Restaurant(nameET.getText().toString(),new Address(cityET.getText().toString(),streetET.getText().toString(),Integer.parseInt(plaqueET.getText().toString()),lat,lang),areaET.getText().toString());
                    QueryMethods.editProfileQuery("rAdmin",sharedPreferences.getString("curUsername",""),restaurant);

                }

                else if(sharedPreferences.getString("userType"," ").equals("driver")){
                    Driver driver = new Driver(0,"","","", UserType.driver,Integer.parseInt(dNidET.getText().toString()),Integer.parseInt(dPlateET.getText().toString()),dNameET.getText().toString(),dLnameET.getText().toString(),dBdET.getText().toString(),dAreaET.getText().toString(),dAreaET.getText().toString());
                    QueryMethods.editProfileQuery("driver",sharedPreferences.getString("curUsername",""),driver);
                }
            }
        });


        changePwBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(QueryMethods.findUserForEditPasswordQuery(sharedPreferences.getString("userType"," "),sharedPreferences.getString("curUsername",""),answerET.getText().toString())==1){
                    QueryMethods.editPasswordQuery(sharedPreferences.getString("userType"," "),sharedPreferences.getString("curUsername",""),newPwET.getText().toString());
                }
            }
        });

    }
}
