package me.foodcharity_dbproject.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.text.DateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import me.foodcharity_dbproject.Database.CharityDataBase;
import me.foodcharity_dbproject.Database.Constants.*;
import me.foodcharity_dbproject.Database.QueryMethods;
import me.foodcharity_dbproject.Entities.Guest;
import me.foodcharity_dbproject.R;


import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;


import static me.foodcharity_dbproject.Database.Constants.*;

public class MainActivity extends AppCompatActivity {

    public final SharedPreferences sharedPreferences = getSharedPreferences(SP_NAME,Context.MODE_PRIVATE);

    Button signupBtn,signinBtn,dltAccBtn,editBtn,readBtn,guestBtn,logoutBtn;
    EditText nameET,lastnameET;


    private void startSignUp(){
        Intent intent = new Intent(this,SignUpactiviy.class);
        startActivity(intent);
    }

    private void startSignIn(){
        Intent intent = new Intent(this,SignInActivity.class);
        startActivity(intent);
    }


    private void startEdit(){
        Intent intent = new Intent(this,EditActivity.class);
        startActivity(intent);
    }


    private void startRead(){
        Intent intent = new Intent(this,ReadDataActivity.class);
        startActivity(intent);
    }


    private void startGuest(){
        Intent intent = new Intent(this, GuestActivity.class);
        startActivity(intent);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        if(sharedPreferences.contains("isLoggedIn")){}
        else {
            sharedPreferences.edit().putBoolean("isLoggedIn",false).commit();
        }

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        CharityDataBase.initialize(this.getBaseContext());
        signupBtn = findViewById(R.id.signUpBtn);
        signinBtn = findViewById(R.id.signInBtn);
        dltAccBtn = findViewById(R.id.dltBtn);
        editBtn = findViewById(R.id.editBtn);
        guestBtn = findViewById(R.id.guestBtn);
        readBtn = findViewById(R.id.readBtn);
        logoutBtn = findViewById(R.id.logoutBtn);

        nameET = findViewById(R.id.gNameET);
        lastnameET = findViewById(R.id.gLNameET);



        signupBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             startSignUp();
            }
        });

        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putBoolean("isLoggedIn",false);
            }
        });

        signinBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferences.getBoolean("isLoggedIn",false)){
                    Toast.makeText(MainActivity.this,"boro fuck you.", Toast.LENGTH_LONG).show();
                }
                else startSignIn();
            }
        });


        dltAccBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferences.getBoolean("isLoggedIn",false)){
                    QueryMethods.deleteUserQuery(sharedPreferences.getString("userType",""),sharedPreferences.getString("curUsername",""));
                    sharedPreferences.edit().putBoolean("isLoggedIn",false);
                }
            }
        });


        editBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(sharedPreferences.getBoolean("isLoggedIn",false)){
                    startEdit();
                }
            }
        });


        readBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPreferences.getBoolean("isLoggedIn",false)){
                    startRead();
                }
            }
        });


        guestBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putString("gName", String.valueOf(nameET.getText())).commit();
                sharedPreferences.edit().putString("gLName", String.valueOf(lastnameET.getText())).commit();
                startGuest();
            }
        });


        initLocation();
        mRequestingLocationUpdates = true;
        startLocationUpdates();

    }





    private Location userLocation;
    private FusedLocationProviderClient fusedLocationClient;
    private SettingsClient settingsClient;
    private LocationRequest locationRequest;
    private LocationSettingsRequest locationSettingsRequest;
    private LocationCallback locationCallback;
    private boolean mRequestingLocationUpdates;
    private String lastUpdateTime;
    private void initLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        settingsClient = LocationServices.getSettingsClient(this);

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                super.onLocationResult(locationResult);
                // location is received
                if (userLocation == null) {
                    userLocation = locationResult.getLastLocation();
                }
                userLocation = locationResult.getLastLocation();
                lastUpdateTime = DateFormat.getTimeInstance().format(new Date());

                onLocationChange();
            }
        };

        mRequestingLocationUpdates = false;

        locationRequest = new LocationRequest();
        locationRequest.setInterval(250);
        locationRequest.setFastestInterval(100);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(locationRequest);
        locationSettingsRequest = builder.build();

    }



    private void onLocationChange() {
        sharedPreferences.edit().putFloat("lat", (float) userLocation.getLatitude()).commit();
        sharedPreferences.edit().putFloat("lang", (float) userLocation.getLongitude()).commit();
    }

    private void startLocationUpdates() {
        settingsClient
                .checkLocationSettings(locationSettingsRequest)
                .addOnSuccessListener(this, new OnSuccessListener<LocationSettingsResponse>() {
                    @SuppressLint("MissingPermission")
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
//                        Log.i(TAG, "All location settings are satisfied.");

                        //noinspection MissingPermission
                        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());

                        onLocationChange();
                    }
                })
                .addOnFailureListener(this, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        int statusCode = ((ApiException) e).getStatusCode();
                        switch (statusCode) {
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
//                                Log.i(TAG, "Location settings are not satisfied. Attempting to upgrade " +
//                                        "location settings ");
                                try {
                                    // Show the dialog by calling startResolutionForResult(), and check the
                                    // result in onActivityResult().
                                    ResolvableApiException rae = (ResolvableApiException) e;
                                    rae.startResolutionForResult(MainActivity.this, 0);
                                } catch (IntentSender.SendIntentException sie) {
//                                    Log.i(TAG, "PendingIntent unable to execute request.");
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                String errorMessage = "Location settings are inadequate, and cannot be " +
                                        "fixed here. Fix in Settings.";
//                                Log.e(TAG, errorMessage);

//                                Toast.makeText(MapActivity.this, errorMessage, Toast.LENGTH_LONG).show();
                        }

                        onLocationChange();
                    }
                });
    }




}