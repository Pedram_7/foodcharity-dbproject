package me.foodcharity_dbproject.Activities;

import androidx.appcompat.app.AppCompatActivity;
import me.foodcharity_dbproject.Database.QueryMethods;
import me.foodcharity_dbproject.Entities.Donation;
import me.foodcharity_dbproject.Entities.Guest;
import me.foodcharity_dbproject.R;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;

import static me.foodcharity_dbproject.Database.Constants.SP_NAME;

public class GuestActivity extends AppCompatActivity {

    RadioGroup charityRG , foodRG;
    Button giveBtn;
    EditText countET;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        ArrayList<String> charityNames , foodNames;

        charityNames = QueryMethods.showCharityListQuery();

        foodNames = QueryMethods.showFoodListQuery();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guest);
        charityRG = findViewById(R.id.charityRG);
        foodRG = findViewById(R.id.foodRG);

        giveBtn = findViewById(R.id.giveBtn);

        for (String s  : foodNames
             ) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(s);
            foodRG.addView(radioButton);

        }



        for (String s  : charityNames
        ) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(s);
            foodRG.addView(radioButton);
        }

        giveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int count = Integer.parseInt(String.valueOf(countET.getText()));
                SharedPreferences sharedPreferences = getSharedPreferences(SP_NAME,MODE_PRIVATE);
                Guest g = new Guest(sharedPreferences.getString("gName",null),sharedPreferences.getString("gLName",null));
                int radioButtonID = foodRG.getCheckedRadioButtonId();
                View radioButton = foodRG.findViewById(radioButtonID);
                int idx = foodRG.indexOfChild(radioButton);
                RadioButton r = (RadioButton) foodRG.getChildAt(idx);
                String selectedtext = r.getText().toString();

                 radioButtonID = charityRG.getCheckedRadioButtonId();
                 radioButton = charityRG.findViewById(radioButtonID);
                 idx = charityRG.indexOfChild(radioButton);
                 r = (RadioButton) charityRG.getChildAt(idx);
                String sss = r.getText().toString();
                QueryMethods.insertDonationQuery(new Donation(selectedtext,sss,sharedPreferences.getString("gName",""),sharedPreferences.getString("gLName",""),5));
            }
        });

        for (String s:charityNames) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(s);
            charityRG.addView(radioButton);
        }

        for (String s:foodNames) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setText(s);
            foodRG.addView(radioButton);
        }

    }
}